﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PcapDotNet.Core;
using PcapDotNet.Core.Extensions;
using PcapDotNet.Packets;
using PcapDotNet.Packets.Ethernet;
using PcapDotNet.Packets.Icmp;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.IpV6;
using PcapDotNet.Packets.Transport;
using System.Text.RegularExpressions;
using System.IO;


namespace GrovixPacketSender
{
    public partial class Form1 : Form
    {
        IList<LivePacketDevice> allDevices;
        Regex macRegex, lineRegex;
        PacketCommunicator communicator;
        PacketManager packetManager;
        public Form1()
        {
            InitializeComponent();

            allDevices = LivePacketDevice.AllLocalMachine;

            if (allDevices.Count == 0)
            {
                MessageBox.Show("No interfaces found! Make sure WinPcap is installed.");
                this.Close();
                return;
            }

            // Print the list
            for (int i = 0; i != allDevices.Count; ++i)
            {
                LivePacketDevice device = allDevices[i];
                string description = "";
                description +=device.Name;

                if(device.Description != null){
                    description += ":";
                    description+=device.Description;
                }
                cbAdapters.Items.Add(description);
            }
            
            if (cbAdapters.Items.Count > 0)
            {
                cbAdapters.SelectedIndex = 0;
                setMacAddresses(allDevices[0]);
            }
            macRegex = new Regex("(([A-Za-z0-9]){2}-){5}([A-Za-z0-9]){2}");
            lineRegex = new Regex("-");

            packetManager = new PacketManager(listBoxPacket);

            comboBoxPrecendence.SelectedIndex = 0;
            comboBoxECN.SelectedIndex = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LivePacketDevice selectedDevice = allDevices[cbAdapters.SelectedIndex];


            PayloadLayer payloadLayer =
                new PayloadLayer
                {
                    Data = new Datagram(Encoding.ASCII.GetBytes(textBoxData.Text)),
                };
            
            EthernetLayer ethernetLayer =
                new EthernetLayer
                {
                    Source = selectedDevice.GetMacAddress(),
                    Destination = new MacAddress((string)macAdrComboBox.Items[macAdrComboBox.SelectedIndex]),
                    EtherType = EthernetType.None, // Will be filled automatically.
                };

            PacketBuilder builder = null;

            string sourceIP = ipAddressSource.Text;

            if (sourceIP == "...")
            {
                MessageBox.Show("Please enter source IP");
                return;
            }

            string destinationIp = ipAddressDestination.Text;
            if (destinationIp == "...")
            {
                MessageBox.Show("Please enter the destination IP");
                return;
            }

            int R = checkBoxReliability.Checked ? 1 : 0;
            int T = checkBoxThroughput.Checked ? 1 : 0;
            int D = checkBoxDelay.Checked ? 1 : 0;
            int Reserved = checkBoxReserved.Checked ? 1 : 0;
            int DF = checkBoxDF.Checked ? 1 : 0;
            int MF = checkBoxMF.Checked ? 1 : 0;
            int Prec = Convert.ToInt32(comboBoxECN.Items[comboBoxECN.SelectedIndex]);
            int ECN = Convert.ToInt32(comboBoxECN.Items[comboBoxECN.SelectedIndex]);
            int typeOfService = (Prec << 5) ^ (D << 4) ^ (T << 3) ^ (R << 2) ^ ECN;
            int fragmentationOptions = (Reserved << 15) ^ (DF << 14) ^ (MF << 13) ^ (Convert.ToInt32(textBoxOffset.Text));
            IpV4Layer ipV4Layer =
                new IpV4Layer
                {
                    Identification = Convert.ToUInt16(textBoxIpIdent.Text),
                    Source = new IpV4Address(sourceIP),
                    CurrentDestination = new IpV4Address(destinationIp),
                    Fragmentation = IpV4Fragmentation.None,
                    HeaderChecksum = null, // Will be filled automatically.
                    Options = IpV4Options.None,
                    Protocol = null, // Will be filled automatically.
                    Ttl = Convert.ToByte(Convert.ToInt16(textBoxTTL.Text)),
                    TypeOfService = Convert.ToByte(typeOfService),
                };
            if (checkBoxIpProtNum.Checked)
            {
                ipV4Layer.Protocol = (IpV4Protocol)Convert.ToInt32(textBoxIpProtNum.Text);
            }
            if (checkBoxIPCS.Checked)
            {
                ipV4Layer.HeaderChecksum = Convert.ToUInt16(textBoxIPCS.Text);
            }
            IcmpEchoLayer icmpLayer = null;
            UdpLayer udpLayer = null;
            TcpLayer tcpLayer = null;

            switch (tabControlProtocol.SelectedIndex)
            {
                case 0:
                    tcpLayer =
                        new TcpLayer
                        {
                            SourcePort = Convert.ToUInt16(textBoxTCPSource.Text),
                            DestinationPort = Convert.ToUInt16(textBoxTCPDestination.Text),
                            SequenceNumber = Convert.ToUInt32(textBoxTCPSeq.Text),
                            AcknowledgmentNumber = Convert.ToUInt32(textBoxTCPAck.Text),
                            Checksum = null,
                            Options = TcpOptions.None,
                            Window = Convert.ToUInt16(textBoxTCPWindow.Text),
                            UrgentPointer = Convert.ToUInt16(textBoxTCPUrgent.Text),
                            ControlBits = TcpControlBits.Acknowledgment,
                        };
                    if (checkBoxTCPCS.Checked)
                    {
                        tcpLayer.Checksum = Convert.ToUInt16(textBoxUDPCS.Text);
                    }
                    builder = new PacketBuilder(ethernetLayer, ipV4Layer, tcpLayer, payloadLayer);
                    break;
                case 1:
                    udpLayer =
                        new UdpLayer
                        {
                            Checksum = null,
                            SourcePort = Convert.ToUInt16(textBoxUDPSource.Text),
                            DestinationPort = Convert.ToUInt16(textBoxUDPDest.Text),
                        };
                    if (checkBoxUDPCS.Checked)
                    {
                        udpLayer.Checksum = Convert.ToUInt16(textBoxUDPCS.Text);
                    }
                    builder = new PacketBuilder(ethernetLayer, ipV4Layer, udpLayer, payloadLayer);
                    break;
                case 2:
                    icmpLayer = 
                        new IcmpEchoLayer
                        {
                            Checksum = null, // Will be filled automatically.
                            Identifier = Convert.ToUInt16(textBoxICMPIdent.Text),
                            SequenceNumber = Convert.ToUInt16(textBoxICMPSeq.Text),
                        };
                    if (checkBoxICMPCS.Checked)
                    {
                        icmpLayer.Checksum = Convert.ToUInt16(textBoxICMPCS.Text);
                    }
                    builder = new PacketBuilder(ethernetLayer, ipV4Layer, icmpLayer, payloadLayer);
                    break;
                default:
                    return;
            }


            Packet packet = builder.Build(DateTime.Now);

            packet.Buffer[14] = Convert.ToByte((Convert.ToInt32(textBoxVersion.Text) << 4) ^ (Convert.ToInt32(textBoxHeaderLength.Text)));
            if (checkBoxIpLen.Checked)
            {
                var ipv4Len = Convert.ToInt32(textBoxIpLen.Text);
                fillUInt16As2byte(packet.Buffer, ipv4Len, 16);
            }
            fillUInt16As2byte(packet.Buffer, fragmentationOptions, 20);

            if (!checkBoxIPCS.Checked)
            {
                calculateCheckSumInBuffer(packet.Buffer, 14, Convert.ToInt32(textBoxHeaderLength.Text) * 4, 24);
            }

            switch (tabControlProtocol.SelectedIndex)
            {
                case 0:
                    //offset
                    int flags = 0;
                    if (checkBoxTCPOffset.Checked)
                    {
                        int offset = Convert.ToInt32(textBoxTCPOffset.Text);
                        offset &= (1 << 4) - 1;
                        flags ^= (offset << 12);
                    }
                    else
                    {
                        flags ^= 5 << 12;
                    }
                    flags ^= (checkBoxR0.Checked ? 1 : 0) << 11; 
                    flags ^= (checkBoxR1.Checked ? 1 : 0) << 10;
                    flags ^= (checkBoxR2.Checked ? 1 : 0) << 9;
                    flags ^= (checkBoxNS.Checked ? 1 : 0) << 8;
                    flags ^= (checkBoxCWR.Checked ? 1 : 0) << 7;
                    flags ^= (checkBoxECE.Checked ? 1 : 0) << 6;
                    flags ^= (checkBoxURG.Checked ? 1 : 0) << 5;
                    flags ^= (checkBoxACK.Checked ? 1 : 0) << 4;
                    flags ^= (checkBoxPSH.Checked ? 1 : 0) << 3;
                    flags ^= (checkBoxRST.Checked ? 1 : 0) << 2;
                    flags ^= (checkBoxSYN.Checked ? 1 : 0) << 1;
                    flags ^= (checkBoxFIN.Checked ? 1 : 0);
                    fillUInt16As2byte(packet.Buffer, flags, 46);
                    if (!checkBoxTCPCS.Checked)
                    {
                        int tcpLen = tcpLayer.Length + payloadLayer.Length;
                        byte[] pseudoHeader = new byte[12 + tcpLen];
                        for (int i = 0; i < 8; ++i)
                        {
                            pseudoHeader[i] = packet.Buffer[26 + i];
                        }
                        pseudoHeader[8] = 0;
                        pseudoHeader[9] = 0x6;
                        fillUInt16As2byte(pseudoHeader, tcpLen, 10);
                        for (int i = 0; i < tcpLen; ++i)
                        {
                            pseudoHeader[12 + i] = packet.Buffer[34 + i];
                        }
                        pseudoHeader[28] = 0;
                        pseudoHeader[29] = 0;
                        calculateCheckSumInBuffer(pseudoHeader, 0, tcpLen + 12, 28);
                        packet.Buffer[50] = pseudoHeader[28];
                        packet.Buffer[51] = pseudoHeader[29];
                    }
                    break;
                case 1:
                    int udpLen = 0;
                    int chosenLen = 0;
                    if (checkBoxUDPLen.Checked)
                    {
                        chosenLen = Convert.ToInt32(textBoxUDPLen.Text);
                        udpLen = Math.Min(chosenLen, udpLayer.Length + payloadLayer.Length);
                        fillUInt16As2byte(packet.Buffer,chosenLen, 38);
                    }
                    else
                    {
                        udpLen = (Convert.ToInt32(packet.Buffer[38]) << 4) + (Convert.ToInt32(packet.Buffer[39]));
                        chosenLen = udpLen;
                    }

                    if (!checkBoxUDPCS.Checked)
                    {
                        byte[] pseudoHeader = new byte[12 + udpLen];
                        for (int i = 0; i < 8; ++i)
                        {
                            pseudoHeader[i] = packet.Buffer[26 + i];
                        }
                        pseudoHeader[8] = 0;
                        pseudoHeader[9] = 0x11;
                        fillUInt16As2byte(pseudoHeader, chosenLen, 10);
                        for (int i = 0; i < udpLen; ++i)
                        {
                            pseudoHeader[12 + i] = packet.Buffer[34 + i];
                        }
                        pseudoHeader[18] = 0;
                        pseudoHeader[19] = 0;
                        calculateCheckSumInBuffer(pseudoHeader, 0, udpLen + 12, 18);
                        packet.Buffer[40] = pseudoHeader[18];
                        packet.Buffer[41] = pseudoHeader[19];
                    }
                    break;
                case 2:
                    packet.Buffer[34] = radioButtonRequest.Checked ? (byte)0x8 : (byte)0;
                    packet.Buffer[35] = Convert.ToByte(Convert.ToUInt16(textBoxICMPCode.Text));
                    if (!checkBoxICMPCS.Checked)
                    {
                        calculateCheckSumInBuffer(packet.Buffer, 34, Convert.ToInt32(icmpLayer.Length)+ payloadLayer.Length, 36);
                    }
                    break;
            }
            if (textBoxPacketName.Text != "")
            {
                packetManager.addPacket(packet, textBoxPacketName.Text);
            }
            else
            {
                MessageBox.Show("Please enter the packet name");
            }
        }

        private void checkBoxIpLen_CheckedChanged(object sender, EventArgs e)
        {
            textBoxIpLen.Enabled = checkBoxIpLen.Checked;
        }

        private void checkBoxIPCS_CheckedChanged(object sender, EventArgs e)
        {
            textBoxIPCS.Enabled = checkBoxIPCS.Checked;
        }

        private void checkBoxICMPCS_CheckedChanged(object sender, EventArgs e)
        {
            textBoxICMPCS.Enabled = checkBoxICMPCS.Checked;
        }

        private void checkBoxUDPCS_CheckedChanged(object sender, EventArgs e)
        {
            textBoxUDPCS.Enabled = checkBoxUDPCS.Checked;
        }

        private void checkBoxTCPCS_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTCPCS.Enabled = checkBoxTCPCS.Checked;
        }

        private void checkBoxUDPLen_CheckedChanged(object sender, EventArgs e)
        {
            textBoxUDPLen.Enabled = checkBoxUDPLen.Checked;
        }

        private void cbAdapters_SelectedIndexChanged(object sender, EventArgs e)
        {
            setMacAddresses(allDevices[cbAdapters.SelectedIndex]);
            communicator = allDevices[cbAdapters.SelectedIndex].Open(100, PacketDeviceOpenAttributes.Promiscuous, 1000);
        }

        private void setMacAddresses(LivePacketDevice selectedDevice)
        {
            macAdrComboBox.Items.Clear();
            macAdrComboBox.Items.Add("ff:ff:ff:ff:ff:ff");
            macAdrComboBox.SelectedIndex = 0;
            int UnicastAddressesCount = selectedDevice.GetNetworkInterface().GetIPProperties().UnicastAddresses.Count;
            if (UnicastAddressesCount > 1)
            {
                ipAddressSource.Text = selectedDevice.GetNetworkInterface().GetIPProperties().UnicastAddresses[1].Address.ToString();
            }

            int GatewayAddressesCount = selectedDevice.GetNetworkInterface().GetIPProperties().GatewayAddresses.Count;
            if (GatewayAddressesCount > 0)
            {
                string a = selectedDevice.GetNetworkInterface().GetIPProperties().GatewayAddresses[0].Address.ToString();
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.UseShellExecute = false;
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.RedirectStandardOutput = true;
                startInfo.RedirectStandardInput = true;
                startInfo.FileName = "cmd.exe";
                process.StartInfo = startInfo;
                process.Start();
                process.StandardInput.WriteLine("arp -a " + a);
                process.StandardInput.Flush();
                process.StandardInput.Close();
                string ab = process.StandardOutput.ReadToEnd();
                Match match = macRegex.Match(ab);
                string mac = match.Value;
                mac = lineRegex.Replace(mac, ":");
                macAdrComboBox.Items.Add(mac);
                macAdrComboBox.SelectedIndex = 1;
            }
        }

        private void buttonSendPacket_Click(object sender, EventArgs e)
        {
            if (listBoxPacket.Items.Count == 0)
            {
                return;
            }
            packetManager.sendPacket(communicator);
        }

        private void checkBoxIpProtNum_CheckedChanged(object sender, EventArgs e)
        {
            textBoxIpProtNum.Enabled = checkBoxIpProtNum.Checked;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            packetManager.deletePacket();
        }

        private uint recalculateChecksum(byte[] buffer, int startIndex, int len)
        {
            uint sum = 0;
            int hdr_len = len;
            int index = startIndex;

            while (hdr_len > 1)
            {
                sum += (Convert.ToUInt32(buffer[index++]) << 8) ^ (Convert.ToUInt32(buffer[index++]));
                if ((sum & 0x80000000) > 0)
                    sum = (sum & 0xFFFF) + (sum >> 16);
                hdr_len -= 2;
            }
            
            if(hdr_len > 0)       /* take care of left over byte */
                sum += (Convert.ToUInt32(buffer[index]) << 8);
            
            while ((sum >> 16)>0)
                sum = (sum & 0xFFFF) + (sum >> 16);

            uint converted = Convert.ToUInt32(sum);
            return ~converted;
        }

        private void calculateCheckSumInBuffer(byte[] buffer, int startIndex, int len, int posInHeader)
        {
            buffer[posInHeader] = 0;
            buffer[posInHeader+1] = 0;
            uint checkSum = recalculateChecksum(buffer, startIndex, len);
            checkSum = checkSum & ((1 << 16) - 1);
            buffer[posInHeader] = Convert.ToByte(checkSum >> 8);
            buffer[posInHeader+1] = Convert.ToByte(checkSum & ((1 << 8) - 1));
        }

        private void fillUInt16As2byte(byte[] buffer, int value, int posInHeader)
        {
            buffer[posInHeader] = Convert.ToByte(value >> 8);
            buffer[posInHeader + 1] = Convert.ToByte(value & ((1 << 8) - 1));
        }

        private void checkBoxTCPOffset_CheckedChanged(object sender, EventArgs e)
        {
            textBoxTCPOffset.Enabled = checkBoxTCPOffset.Checked;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.FileName = packetManager.vListOfNamedPackets[listBoxPacket.SelectedIndex].name;
            if (sfd.ShowDialog() == DialogResult.OK)
                File.WriteAllBytes(sfd.FileName, packetManager.vListOfNamedPackets[listBoxPacket.SelectedIndex].packet.Buffer);
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            byte[] buffer = null;
            string name = "";
            Packet packet = null;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                buffer = File.ReadAllBytes(ofd.FileName);
                name = ofd.FileName;
                packet = new Packet(buffer, DateTime.Now, DataLinkKind.Ethernet);
                packetManager.addPacket(packet, name);
            }
        }

        private void buttonSendAll_Click(object sender, EventArgs e)
        {
            int delay = Convert.ToInt32(textBoxDelay.Text);
            int times = Convert.ToInt32(textBoxNumOfRepeat.Text);
            for (int i = 0; i < times; ++i)
            {
                foreach (var element in packetManager.vListOfNamedPackets)
                {
                    communicator.SendPacket(element.packet);
                    System.Threading.Thread.Sleep(delay);
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            packetManager.vListOfNamedPackets.Clear();
            listBoxPacket.Items.Clear();
            listBoxPacket.Update();
        }
    }
}
//начальная работа станции в сети -MAC адрес
//ip адрес default route
//DNS
//NAT, ARP, RARP
//Multicast router
//DHCP