﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PcapDotNet.Packets;
using System.Windows.Forms;
using PcapDotNet.Core;

namespace GrovixPacketSender
{
    struct NamedPacket
    {
        public Packet packet;
        public string name;
        public NamedPacket(Packet packet, string name)
        {
            this.packet = packet;
            this.name = name;
        }
    }
    class PacketManager
    {
        public List<NamedPacket> vListOfNamedPackets;
        public ListBox listBoxPacket;
        public PacketManager(ListBox listBoxPacket)
        {
            this.listBoxPacket = listBoxPacket;
            vListOfNamedPackets = new List<NamedPacket>();
        }
        public void addPacket(Packet packet, string name)
        {
            vListOfNamedPackets.Add(new NamedPacket(packet, name));
            listBoxPacket.Items.Add(name);
        }

        public void deletePacket()
        {
            int index = listBoxPacket.SelectedIndex;
            listBoxPacket.Items.RemoveAt(index);
            vListOfNamedPackets.RemoveAt(index);
            listBoxPacket.Update();
        }

        public void sendPacket(PacketCommunicator communicator)
        {
            int index = listBoxPacket.SelectedIndex;
            communicator.SendPacket(vListOfNamedPackets[index].packet);
        }
    }
}
