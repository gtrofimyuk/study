﻿namespace GrovixPacketSender
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbAdapters = new System.Windows.Forms.ComboBox();
            this.buttonBuild = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPageIP = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxOffset = new System.Windows.Forms.TextBox();
            this.textBoxIpProtNum = new System.Windows.Forms.TextBox();
            this.checkBoxIpProtNum = new System.Windows.Forms.CheckBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.ipAddressDestination = new IPAddressControlLib.IPAddressControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.ipAddressSource = new IPAddressControlLib.IPAddressControl();
            this.checkBoxIPCS = new System.Windows.Forms.CheckBox();
            this.textBoxIPCS = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBoxECN = new System.Windows.Forms.ComboBox();
            this.comboBoxPrecendence = new System.Windows.Forms.ComboBox();
            this.checkBoxReliability = new System.Windows.Forms.CheckBox();
            this.checkBoxThroughput = new System.Windows.Forms.CheckBox();
            this.checkBoxDelay = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxTTL = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBoxMF = new System.Windows.Forms.CheckBox();
            this.checkBoxDF = new System.Windows.Forms.CheckBox();
            this.checkBoxReserved = new System.Windows.Forms.CheckBox();
            this.textBoxIpIdent = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxIpLen = new System.Windows.Forms.TextBox();
            this.checkBoxIpLen = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxHeaderLength = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxVersion = new System.Windows.Forms.TextBox();
            this.tabControlProtocol = new System.Windows.Forms.TabControl();
            this.tabPageTCP = new System.Windows.Forms.TabPage();
            this.checkBoxTCPOffset = new System.Windows.Forms.CheckBox();
            this.checkBoxNS = new System.Windows.Forms.CheckBox();
            this.textBoxTCPUrgent = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxTCPCS = new System.Windows.Forms.TextBox();
            this.textBoxTCPWindow = new System.Windows.Forms.TextBox();
            this.checkBoxTCPCS = new System.Windows.Forms.CheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.checkBoxECE = new System.Windows.Forms.CheckBox();
            this.checkBoxCWR = new System.Windows.Forms.CheckBox();
            this.checkBoxFIN = new System.Windows.Forms.CheckBox();
            this.checkBoxSYN = new System.Windows.Forms.CheckBox();
            this.checkBoxRST = new System.Windows.Forms.CheckBox();
            this.checkBoxPSH = new System.Windows.Forms.CheckBox();
            this.checkBoxACK = new System.Windows.Forms.CheckBox();
            this.checkBoxURG = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.checkBoxR2 = new System.Windows.Forms.CheckBox();
            this.checkBoxR1 = new System.Windows.Forms.CheckBox();
            this.checkBoxR0 = new System.Windows.Forms.CheckBox();
            this.textBoxTCPOffset = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxTCPAck = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxTCPSeq = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxTCPDestination = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxTCPSource = new System.Windows.Forms.TextBox();
            this.tabPageUDP = new System.Windows.Forms.TabPage();
            this.checkBoxUDPCS = new System.Windows.Forms.CheckBox();
            this.textBoxUDPLen = new System.Windows.Forms.TextBox();
            this.textBoxUDPCS = new System.Windows.Forms.TextBox();
            this.checkBoxUDPLen = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxUDPDest = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxUDPSource = new System.Windows.Forms.TextBox();
            this.tabPageICMP = new System.Windows.Forms.TabPage();
            this.label20 = new System.Windows.Forms.Label();
            this.textBoxICMPCode = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBoxICMPSeq = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxICMPIdent = new System.Windows.Forms.TextBox();
            this.checkBoxICMPCS = new System.Windows.Forms.CheckBox();
            this.textBoxICMPCS = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButtonRequest = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxData = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Repeat = new System.Windows.Forms.Label();
            this.textBoxNumOfRepeat = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxDelay = new System.Windows.Forms.TextBox();
            this.buttonSendAll = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBoxPacketName = new System.Windows.Forms.TextBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonSendPacket = new System.Windows.Forms.Button();
            this.listBoxPacket = new System.Windows.Forms.ListBox();
            this.groupMac = new System.Windows.Forms.GroupBox();
            this.macAdrComboBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPageIP.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabControlProtocol.SuspendLayout();
            this.tabPageTCP.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tabPageUDP.SuspendLayout();
            this.tabPageICMP.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupMac.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbAdapters
            // 
            this.cbAdapters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbAdapters.Location = new System.Drawing.Point(0, 15);
            this.cbAdapters.Name = "cbAdapters";
            this.cbAdapters.Size = new System.Drawing.Size(618, 21);
            this.cbAdapters.TabIndex = 5;
            this.cbAdapters.SelectedIndexChanged += new System.EventHandler(this.cbAdapters_SelectedIndexChanged);
            // 
            // buttonBuild
            // 
            this.buttonBuild.Location = new System.Drawing.Point(6, 41);
            this.buttonBuild.Name = "buttonBuild";
            this.buttonBuild.Size = new System.Drawing.Size(75, 23);
            this.buttonBuild.TabIndex = 7;
            this.buttonBuild.Text = "Build";
            this.buttonBuild.UseVisualStyleBackColor = true;
            this.buttonBuild.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPageIP);
            this.tabControl1.Location = new System.Drawing.Point(12, 58);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(220, 530);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPageIP
            // 
            this.tabPageIP.BackColor = System.Drawing.Color.Transparent;
            this.tabPageIP.Controls.Add(this.label19);
            this.tabPageIP.Controls.Add(this.textBoxOffset);
            this.tabPageIP.Controls.Add(this.textBoxIpProtNum);
            this.tabPageIP.Controls.Add(this.checkBoxIpProtNum);
            this.tabPageIP.Controls.Add(this.groupBox8);
            this.tabPageIP.Controls.Add(this.groupBox7);
            this.tabPageIP.Controls.Add(this.checkBoxIPCS);
            this.tabPageIP.Controls.Add(this.textBoxIPCS);
            this.tabPageIP.Controls.Add(this.groupBox4);
            this.tabPageIP.Controls.Add(this.textBoxTTL);
            this.tabPageIP.Controls.Add(this.label7);
            this.tabPageIP.Controls.Add(this.groupBox5);
            this.tabPageIP.Controls.Add(this.textBoxIpIdent);
            this.tabPageIP.Controls.Add(this.label6);
            this.tabPageIP.Controls.Add(this.textBoxIpLen);
            this.tabPageIP.Controls.Add(this.checkBoxIpLen);
            this.tabPageIP.Controls.Add(this.label3);
            this.tabPageIP.Controls.Add(this.textBoxHeaderLength);
            this.tabPageIP.Controls.Add(this.label2);
            this.tabPageIP.Controls.Add(this.label1);
            this.tabPageIP.Controls.Add(this.textBoxVersion);
            this.tabPageIP.Location = new System.Drawing.Point(4, 22);
            this.tabPageIP.Name = "tabPageIP";
            this.tabPageIP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageIP.Size = new System.Drawing.Size(212, 504);
            this.tabPageIP.TabIndex = 1;
            this.tabPageIP.Text = "IP";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 312);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(35, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "Offset";
            // 
            // textBoxOffset
            // 
            this.textBoxOffset.Location = new System.Drawing.Point(129, 309);
            this.textBoxOffset.Name = "textBoxOffset";
            this.textBoxOffset.Size = new System.Drawing.Size(71, 20);
            this.textBoxOffset.TabIndex = 30;
            this.textBoxOffset.Text = "0";
            this.textBoxOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxIpProtNum
            // 
            this.textBoxIpProtNum.Enabled = false;
            this.textBoxIpProtNum.Location = new System.Drawing.Point(143, 353);
            this.textBoxIpProtNum.Name = "textBoxIpProtNum";
            this.textBoxIpProtNum.Size = new System.Drawing.Size(57, 20);
            this.textBoxIpProtNum.TabIndex = 18;
            this.textBoxIpProtNum.Text = "0";
            this.textBoxIpProtNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxIpProtNum
            // 
            this.checkBoxIpProtNum.AutoSize = true;
            this.checkBoxIpProtNum.Location = new System.Drawing.Point(9, 355);
            this.checkBoxIpProtNum.Name = "checkBoxIpProtNum";
            this.checkBoxIpProtNum.Size = new System.Drawing.Size(65, 17);
            this.checkBoxIpProtNum.TabIndex = 17;
            this.checkBoxIpProtNum.Text = "Protocol";
            this.checkBoxIpProtNum.UseVisualStyleBackColor = true;
            this.checkBoxIpProtNum.CheckedChanged += new System.EventHandler(this.checkBoxIpProtNum_CheckedChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.ipAddressDestination);
            this.groupBox8.Location = new System.Drawing.Point(20, 455);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(150, 45);
            this.groupBox8.TabIndex = 16;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Destination IP";
            // 
            // ipAddressDestination
            // 
            this.ipAddressDestination.AllowInternalTab = false;
            this.ipAddressDestination.AutoHeight = true;
            this.ipAddressDestination.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressDestination.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressDestination.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressDestination.Location = new System.Drawing.Point(10, 19);
            this.ipAddressDestination.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressDestination.Name = "ipAddressDestination";
            this.ipAddressDestination.ReadOnly = false;
            this.ipAddressDestination.Size = new System.Drawing.Size(122, 20);
            this.ipAddressDestination.TabIndex = 2;
            this.ipAddressDestination.Text = "169.254.24.209";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.ipAddressSource);
            this.groupBox7.Location = new System.Drawing.Point(20, 405);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(150, 45);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Source IP";
            // 
            // ipAddressSource
            // 
            this.ipAddressSource.AllowInternalTab = false;
            this.ipAddressSource.AutoHeight = true;
            this.ipAddressSource.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressSource.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressSource.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ipAddressSource.Location = new System.Drawing.Point(10, 19);
            this.ipAddressSource.MinimumSize = new System.Drawing.Size(87, 20);
            this.ipAddressSource.Name = "ipAddressSource";
            this.ipAddressSource.ReadOnly = false;
            this.ipAddressSource.Size = new System.Drawing.Size(122, 20);
            this.ipAddressSource.TabIndex = 2;
            this.ipAddressSource.Text = "...";
            // 
            // checkBoxIPCS
            // 
            this.checkBoxIPCS.AutoSize = true;
            this.checkBoxIPCS.Location = new System.Drawing.Point(9, 379);
            this.checkBoxIPCS.Name = "checkBoxIPCS";
            this.checkBoxIPCS.Size = new System.Drawing.Size(114, 17);
            this.checkBoxIPCS.TabIndex = 14;
            this.checkBoxIPCS.Text = "Header Checksum";
            this.checkBoxIPCS.UseVisualStyleBackColor = true;
            this.checkBoxIPCS.CheckedChanged += new System.EventHandler(this.checkBoxIPCS_CheckedChanged);
            // 
            // textBoxIPCS
            // 
            this.textBoxIPCS.Enabled = false;
            this.textBoxIPCS.Location = new System.Drawing.Point(129, 379);
            this.textBoxIPCS.Name = "textBoxIPCS";
            this.textBoxIPCS.Size = new System.Drawing.Size(71, 20);
            this.textBoxIPCS.TabIndex = 13;
            this.textBoxIPCS.Text = "0";
            this.textBoxIPCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBoxECN);
            this.groupBox4.Controls.Add(this.comboBoxPrecendence);
            this.groupBox4.Controls.Add(this.checkBoxReliability);
            this.groupBox4.Controls.Add(this.checkBoxThroughput);
            this.groupBox4.Controls.Add(this.checkBoxDelay);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(3, 52);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(200, 116);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Type of Service";
            // 
            // comboBoxECN
            // 
            this.comboBoxECN.FormattingEnabled = true;
            this.comboBoxECN.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3"});
            this.comboBoxECN.Location = new System.Drawing.Point(153, 16);
            this.comboBoxECN.Name = "comboBoxECN";
            this.comboBoxECN.Size = new System.Drawing.Size(41, 21);
            this.comboBoxECN.TabIndex = 12;
            // 
            // comboBoxPrecendence
            // 
            this.comboBoxPrecendence.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7"});
            this.comboBoxPrecendence.Location = new System.Drawing.Point(80, 16);
            this.comboBoxPrecendence.Name = "comboBoxPrecendence";
            this.comboBoxPrecendence.Size = new System.Drawing.Size(41, 21);
            this.comboBoxPrecendence.TabIndex = 11;
            // 
            // checkBoxReliability
            // 
            this.checkBoxReliability.AutoSize = true;
            this.checkBoxReliability.Location = new System.Drawing.Point(14, 90);
            this.checkBoxReliability.Name = "checkBoxReliability";
            this.checkBoxReliability.Size = new System.Drawing.Size(70, 17);
            this.checkBoxReliability.TabIndex = 10;
            this.checkBoxReliability.Text = "Reliability";
            this.checkBoxReliability.UseVisualStyleBackColor = true;
            // 
            // checkBoxThroughput
            // 
            this.checkBoxThroughput.AutoSize = true;
            this.checkBoxThroughput.Location = new System.Drawing.Point(14, 67);
            this.checkBoxThroughput.Name = "checkBoxThroughput";
            this.checkBoxThroughput.Size = new System.Drawing.Size(81, 17);
            this.checkBoxThroughput.TabIndex = 9;
            this.checkBoxThroughput.Text = "Throughput";
            this.checkBoxThroughput.UseVisualStyleBackColor = true;
            // 
            // checkBoxDelay
            // 
            this.checkBoxDelay.AutoSize = true;
            this.checkBoxDelay.Location = new System.Drawing.Point(14, 44);
            this.checkBoxDelay.Name = "checkBoxDelay";
            this.checkBoxDelay.Size = new System.Drawing.Size(53, 17);
            this.checkBoxDelay.TabIndex = 8;
            this.checkBoxDelay.Text = "Delay";
            this.checkBoxDelay.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(127, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "ECN";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Precendence";
            // 
            // textBoxTTL
            // 
            this.textBoxTTL.Location = new System.Drawing.Point(143, 330);
            this.textBoxTTL.Name = "textBoxTTL";
            this.textBoxTTL.Size = new System.Drawing.Size(57, 20);
            this.textBoxTTL.TabIndex = 12;
            this.textBoxTTL.Text = "128";
            this.textBoxTTL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Time to Live";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBoxMF);
            this.groupBox5.Controls.Add(this.checkBoxDF);
            this.groupBox5.Controls.Add(this.checkBoxReserved);
            this.groupBox5.Location = new System.Drawing.Point(10, 218);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(196, 85);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Flags";
            // 
            // checkBoxMF
            // 
            this.checkBoxMF.AutoSize = true;
            this.checkBoxMF.Location = new System.Drawing.Point(3, 66);
            this.checkBoxMF.Name = "checkBoxMF";
            this.checkBoxMF.Size = new System.Drawing.Size(102, 17);
            this.checkBoxMF.TabIndex = 2;
            this.checkBoxMF.Text = "More Fragments";
            this.checkBoxMF.UseVisualStyleBackColor = true;
            // 
            // checkBoxDF
            // 
            this.checkBoxDF.AutoSize = true;
            this.checkBoxDF.Location = new System.Drawing.Point(3, 43);
            this.checkBoxDF.Name = "checkBoxDF";
            this.checkBoxDF.Size = new System.Drawing.Size(98, 17);
            this.checkBoxDF.TabIndex = 1;
            this.checkBoxDF.Text = "Don\'t Fragment";
            this.checkBoxDF.UseVisualStyleBackColor = true;
            // 
            // checkBoxReserved
            // 
            this.checkBoxReserved.AutoSize = true;
            this.checkBoxReserved.Location = new System.Drawing.Point(3, 20);
            this.checkBoxReserved.Name = "checkBoxReserved";
            this.checkBoxReserved.Size = new System.Drawing.Size(72, 17);
            this.checkBoxReserved.TabIndex = 0;
            this.checkBoxReserved.Text = "Reserved";
            this.checkBoxReserved.UseVisualStyleBackColor = true;
            // 
            // textBoxIpIdent
            // 
            this.textBoxIpIdent.Location = new System.Drawing.Point(129, 197);
            this.textBoxIpIdent.Name = "textBoxIpIdent";
            this.textBoxIpIdent.Size = new System.Drawing.Size(71, 20);
            this.textBoxIpIdent.TabIndex = 9;
            this.textBoxIpIdent.Text = "1";
            this.textBoxIpIdent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Identification";
            // 
            // textBoxIpLen
            // 
            this.textBoxIpLen.Enabled = false;
            this.textBoxIpLen.Location = new System.Drawing.Point(144, 171);
            this.textBoxIpLen.Name = "textBoxIpLen";
            this.textBoxIpLen.Size = new System.Drawing.Size(56, 20);
            this.textBoxIpLen.TabIndex = 7;
            this.textBoxIpLen.Text = "0";
            this.textBoxIpLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxIpLen
            // 
            this.checkBoxIpLen.AutoSize = true;
            this.checkBoxIpLen.Location = new System.Drawing.Point(10, 174);
            this.checkBoxIpLen.Name = "checkBoxIpLen";
            this.checkBoxIpLen.Size = new System.Drawing.Size(86, 17);
            this.checkBoxIpLen.TabIndex = 6;
            this.checkBoxIpLen.Text = "Total Length";
            this.checkBoxIpLen.UseVisualStyleBackColor = true;
            this.checkBoxIpLen.CheckedChanged += new System.EventHandler(this.checkBoxIpLen_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Header Length";
            // 
            // textBoxHeaderLength
            // 
            this.textBoxHeaderLength.Location = new System.Drawing.Point(177, 26);
            this.textBoxHeaderLength.Name = "textBoxHeaderLength";
            this.textBoxHeaderLength.Size = new System.Drawing.Size(23, 20);
            this.textBoxHeaderLength.TabIndex = 3;
            this.textBoxHeaderLength.Text = "5";
            this.textBoxHeaderLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "IP Version";
            // 
            // textBoxVersion
            // 
            this.textBoxVersion.Location = new System.Drawing.Point(177, 4);
            this.textBoxVersion.Name = "textBoxVersion";
            this.textBoxVersion.Size = new System.Drawing.Size(23, 20);
            this.textBoxVersion.TabIndex = 0;
            this.textBoxVersion.Text = "4";
            this.textBoxVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabControlProtocol
            // 
            this.tabControlProtocol.Controls.Add(this.tabPageTCP);
            this.tabControlProtocol.Controls.Add(this.tabPageUDP);
            this.tabControlProtocol.Controls.Add(this.tabPageICMP);
            this.tabControlProtocol.Location = new System.Drawing.Point(238, 58);
            this.tabControlProtocol.Name = "tabControlProtocol";
            this.tabControlProtocol.SelectedIndex = 0;
            this.tabControlProtocol.Size = new System.Drawing.Size(200, 397);
            this.tabControlProtocol.TabIndex = 9;
            // 
            // tabPageTCP
            // 
            this.tabPageTCP.BackColor = System.Drawing.Color.Transparent;
            this.tabPageTCP.Controls.Add(this.checkBoxTCPOffset);
            this.tabPageTCP.Controls.Add(this.checkBoxNS);
            this.tabPageTCP.Controls.Add(this.textBoxTCPUrgent);
            this.tabPageTCP.Controls.Add(this.label14);
            this.tabPageTCP.Controls.Add(this.textBoxTCPCS);
            this.tabPageTCP.Controls.Add(this.textBoxTCPWindow);
            this.tabPageTCP.Controls.Add(this.checkBoxTCPCS);
            this.tabPageTCP.Controls.Add(this.label13);
            this.tabPageTCP.Controls.Add(this.groupBox10);
            this.tabPageTCP.Controls.Add(this.groupBox9);
            this.tabPageTCP.Controls.Add(this.textBoxTCPOffset);
            this.tabPageTCP.Controls.Add(this.label10);
            this.tabPageTCP.Controls.Add(this.textBoxTCPAck);
            this.tabPageTCP.Controls.Add(this.label11);
            this.tabPageTCP.Controls.Add(this.textBoxTCPSeq);
            this.tabPageTCP.Controls.Add(this.label9);
            this.tabPageTCP.Controls.Add(this.textBoxTCPDestination);
            this.tabPageTCP.Controls.Add(this.label8);
            this.tabPageTCP.Controls.Add(this.textBoxTCPSource);
            this.tabPageTCP.Location = new System.Drawing.Point(4, 22);
            this.tabPageTCP.Name = "tabPageTCP";
            this.tabPageTCP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTCP.Size = new System.Drawing.Size(192, 371);
            this.tabPageTCP.TabIndex = 0;
            this.tabPageTCP.Text = "TCP";
            // 
            // checkBoxTCPOffset
            // 
            this.checkBoxTCPOffset.AutoSize = true;
            this.checkBoxTCPOffset.Location = new System.Drawing.Point(9, 106);
            this.checkBoxTCPOffset.Name = "checkBoxTCPOffset";
            this.checkBoxTCPOffset.Size = new System.Drawing.Size(54, 17);
            this.checkBoxTCPOffset.TabIndex = 30;
            this.checkBoxTCPOffset.Text = "Offset";
            this.checkBoxTCPOffset.UseVisualStyleBackColor = true;
            this.checkBoxTCPOffset.CheckedChanged += new System.EventHandler(this.checkBoxTCPOffset_CheckedChanged);
            // 
            // checkBoxNS
            // 
            this.checkBoxNS.AutoSize = true;
            this.checkBoxNS.Location = new System.Drawing.Point(111, 142);
            this.checkBoxNS.Name = "checkBoxNS";
            this.checkBoxNS.Size = new System.Drawing.Size(41, 17);
            this.checkBoxNS.TabIndex = 8;
            this.checkBoxNS.Text = "NS";
            this.checkBoxNS.UseVisualStyleBackColor = true;
            // 
            // textBoxTCPUrgent
            // 
            this.textBoxTCPUrgent.Location = new System.Drawing.Point(129, 344);
            this.textBoxTCPUrgent.Name = "textBoxTCPUrgent";
            this.textBoxTCPUrgent.Size = new System.Drawing.Size(56, 20);
            this.textBoxTCPUrgent.TabIndex = 29;
            this.textBoxTCPUrgent.Text = "0";
            this.textBoxTCPUrgent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 347);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Urgent Pointer";
            // 
            // textBoxTCPCS
            // 
            this.textBoxTCPCS.Enabled = false;
            this.textBoxTCPCS.Location = new System.Drawing.Point(129, 318);
            this.textBoxTCPCS.Name = "textBoxTCPCS";
            this.textBoxTCPCS.Size = new System.Drawing.Size(56, 20);
            this.textBoxTCPCS.TabIndex = 18;
            this.textBoxTCPCS.Text = "0";
            this.textBoxTCPCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxTCPWindow
            // 
            this.textBoxTCPWindow.Location = new System.Drawing.Point(129, 293);
            this.textBoxTCPWindow.Name = "textBoxTCPWindow";
            this.textBoxTCPWindow.Size = new System.Drawing.Size(56, 20);
            this.textBoxTCPWindow.TabIndex = 27;
            this.textBoxTCPWindow.Text = "65535";
            this.textBoxTCPWindow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxTCPCS
            // 
            this.checkBoxTCPCS.AutoSize = true;
            this.checkBoxTCPCS.Location = new System.Drawing.Point(10, 318);
            this.checkBoxTCPCS.Name = "checkBoxTCPCS";
            this.checkBoxTCPCS.Size = new System.Drawing.Size(76, 17);
            this.checkBoxTCPCS.TabIndex = 17;
            this.checkBoxTCPCS.Text = "Checksum";
            this.checkBoxTCPCS.UseVisualStyleBackColor = true;
            this.checkBoxTCPCS.CheckedChanged += new System.EventHandler(this.checkBoxTCPCS_CheckedChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 296);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Window Size";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.checkBoxECE);
            this.groupBox10.Controls.Add(this.checkBoxCWR);
            this.groupBox10.Controls.Add(this.checkBoxFIN);
            this.groupBox10.Controls.Add(this.checkBoxSYN);
            this.groupBox10.Controls.Add(this.checkBoxRST);
            this.groupBox10.Controls.Add(this.checkBoxPSH);
            this.groupBox10.Controls.Add(this.checkBoxACK);
            this.groupBox10.Controls.Add(this.checkBoxURG);
            this.groupBox10.Location = new System.Drawing.Point(13, 171);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(171, 107);
            this.groupBox10.TabIndex = 25;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Flags";
            // 
            // checkBoxECE
            // 
            this.checkBoxECE.AutoSize = true;
            this.checkBoxECE.Location = new System.Drawing.Point(85, 19);
            this.checkBoxECE.Name = "checkBoxECE";
            this.checkBoxECE.Size = new System.Drawing.Size(47, 17);
            this.checkBoxECE.TabIndex = 7;
            this.checkBoxECE.Text = "ECE";
            this.checkBoxECE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.checkBoxECE.UseVisualStyleBackColor = true;
            // 
            // checkBoxCWR
            // 
            this.checkBoxCWR.AutoSize = true;
            this.checkBoxCWR.Location = new System.Drawing.Point(4, 19);
            this.checkBoxCWR.Name = "checkBoxCWR";
            this.checkBoxCWR.Size = new System.Drawing.Size(52, 17);
            this.checkBoxCWR.TabIndex = 6;
            this.checkBoxCWR.Text = "CWR";
            this.checkBoxCWR.UseVisualStyleBackColor = true;
            // 
            // checkBoxFIN
            // 
            this.checkBoxFIN.AutoSize = true;
            this.checkBoxFIN.Location = new System.Drawing.Point(85, 88);
            this.checkBoxFIN.Name = "checkBoxFIN";
            this.checkBoxFIN.Size = new System.Drawing.Size(43, 17);
            this.checkBoxFIN.TabIndex = 5;
            this.checkBoxFIN.Text = "FIN";
            this.checkBoxFIN.UseVisualStyleBackColor = true;
            // 
            // checkBoxSYN
            // 
            this.checkBoxSYN.AutoSize = true;
            this.checkBoxSYN.Location = new System.Drawing.Point(85, 65);
            this.checkBoxSYN.Name = "checkBoxSYN";
            this.checkBoxSYN.Size = new System.Drawing.Size(48, 17);
            this.checkBoxSYN.TabIndex = 4;
            this.checkBoxSYN.Text = "SYN";
            this.checkBoxSYN.UseVisualStyleBackColor = true;
            // 
            // checkBoxRST
            // 
            this.checkBoxRST.AutoSize = true;
            this.checkBoxRST.Location = new System.Drawing.Point(85, 42);
            this.checkBoxRST.Name = "checkBoxRST";
            this.checkBoxRST.Size = new System.Drawing.Size(48, 17);
            this.checkBoxRST.TabIndex = 3;
            this.checkBoxRST.Text = "RST";
            this.checkBoxRST.UseVisualStyleBackColor = true;
            // 
            // checkBoxPSH
            // 
            this.checkBoxPSH.AutoSize = true;
            this.checkBoxPSH.Location = new System.Drawing.Point(4, 88);
            this.checkBoxPSH.Name = "checkBoxPSH";
            this.checkBoxPSH.Size = new System.Drawing.Size(48, 17);
            this.checkBoxPSH.TabIndex = 2;
            this.checkBoxPSH.Text = "PSH";
            this.checkBoxPSH.UseVisualStyleBackColor = true;
            // 
            // checkBoxACK
            // 
            this.checkBoxACK.AutoSize = true;
            this.checkBoxACK.Location = new System.Drawing.Point(4, 65);
            this.checkBoxACK.Name = "checkBoxACK";
            this.checkBoxACK.Size = new System.Drawing.Size(47, 17);
            this.checkBoxACK.TabIndex = 1;
            this.checkBoxACK.Text = "ACK";
            this.checkBoxACK.UseVisualStyleBackColor = true;
            // 
            // checkBoxURG
            // 
            this.checkBoxURG.AutoSize = true;
            this.checkBoxURG.Location = new System.Drawing.Point(4, 42);
            this.checkBoxURG.Name = "checkBoxURG";
            this.checkBoxURG.Size = new System.Drawing.Size(50, 17);
            this.checkBoxURG.TabIndex = 0;
            this.checkBoxURG.Text = "URG";
            this.checkBoxURG.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.checkBoxR2);
            this.groupBox9.Controls.Add(this.checkBoxR1);
            this.groupBox9.Controls.Add(this.checkBoxR0);
            this.groupBox9.Location = new System.Drawing.Point(9, 126);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(96, 40);
            this.groupBox9.TabIndex = 24;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Reserved Bits";
            // 
            // checkBoxR2
            // 
            this.checkBoxR2.AutoSize = true;
            this.checkBoxR2.Location = new System.Drawing.Point(49, 19);
            this.checkBoxR2.Name = "checkBoxR2";
            this.checkBoxR2.Size = new System.Drawing.Size(15, 14);
            this.checkBoxR2.TabIndex = 2;
            this.checkBoxR2.UseVisualStyleBackColor = true;
            // 
            // checkBoxR1
            // 
            this.checkBoxR1.AutoSize = true;
            this.checkBoxR1.Location = new System.Drawing.Point(28, 19);
            this.checkBoxR1.Name = "checkBoxR1";
            this.checkBoxR1.Size = new System.Drawing.Size(15, 14);
            this.checkBoxR1.TabIndex = 1;
            this.checkBoxR1.UseVisualStyleBackColor = true;
            // 
            // checkBoxR0
            // 
            this.checkBoxR0.AutoSize = true;
            this.checkBoxR0.Location = new System.Drawing.Point(7, 19);
            this.checkBoxR0.Name = "checkBoxR0";
            this.checkBoxR0.Size = new System.Drawing.Size(15, 14);
            this.checkBoxR0.TabIndex = 0;
            this.checkBoxR0.UseVisualStyleBackColor = true;
            // 
            // textBoxTCPOffset
            // 
            this.textBoxTCPOffset.Enabled = false;
            this.textBoxTCPOffset.Location = new System.Drawing.Point(128, 103);
            this.textBoxTCPOffset.Name = "textBoxTCPOffset";
            this.textBoxTCPOffset.Size = new System.Drawing.Size(57, 20);
            this.textBoxTCPOffset.TabIndex = 17;
            this.textBoxTCPOffset.Text = "5";
            this.textBoxTCPOffset.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "Ack number";
            // 
            // textBoxTCPAck
            // 
            this.textBoxTCPAck.Location = new System.Drawing.Point(85, 78);
            this.textBoxTCPAck.Name = "textBoxTCPAck";
            this.textBoxTCPAck.Size = new System.Drawing.Size(101, 20);
            this.textBoxTCPAck.TabIndex = 23;
            this.textBoxTCPAck.Text = "0";
            this.textBoxTCPAck.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "Seq number";
            // 
            // textBoxTCPSeq
            // 
            this.textBoxTCPSeq.Location = new System.Drawing.Point(85, 55);
            this.textBoxTCPSeq.Name = "textBoxTCPSeq";
            this.textBoxTCPSeq.Size = new System.Drawing.Size(101, 20);
            this.textBoxTCPSeq.TabIndex = 21;
            this.textBoxTCPSeq.Text = "0";
            this.textBoxTCPSeq.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 33);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(81, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Destination port";
            // 
            // textBoxTCPDestination
            // 
            this.textBoxTCPDestination.Location = new System.Drawing.Point(124, 29);
            this.textBoxTCPDestination.Name = "textBoxTCPDestination";
            this.textBoxTCPDestination.Size = new System.Drawing.Size(62, 20);
            this.textBoxTCPDestination.TabIndex = 19;
            this.textBoxTCPDestination.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Source port";
            // 
            // textBoxTCPSource
            // 
            this.textBoxTCPSource.Location = new System.Drawing.Point(124, 6);
            this.textBoxTCPSource.Name = "textBoxTCPSource";
            this.textBoxTCPSource.Size = new System.Drawing.Size(62, 20);
            this.textBoxTCPSource.TabIndex = 17;
            this.textBoxTCPSource.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPageUDP
            // 
            this.tabPageUDP.BackColor = System.Drawing.Color.Transparent;
            this.tabPageUDP.Controls.Add(this.checkBoxUDPCS);
            this.tabPageUDP.Controls.Add(this.textBoxUDPLen);
            this.tabPageUDP.Controls.Add(this.textBoxUDPCS);
            this.tabPageUDP.Controls.Add(this.checkBoxUDPLen);
            this.tabPageUDP.Controls.Add(this.label15);
            this.tabPageUDP.Controls.Add(this.textBoxUDPDest);
            this.tabPageUDP.Controls.Add(this.label16);
            this.tabPageUDP.Controls.Add(this.textBoxUDPSource);
            this.tabPageUDP.Location = new System.Drawing.Point(4, 22);
            this.tabPageUDP.Name = "tabPageUDP";
            this.tabPageUDP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageUDP.Size = new System.Drawing.Size(192, 371);
            this.tabPageUDP.TabIndex = 1;
            this.tabPageUDP.Text = "UDP";
            // 
            // checkBoxUDPCS
            // 
            this.checkBoxUDPCS.AutoSize = true;
            this.checkBoxUDPCS.Location = new System.Drawing.Point(3, 81);
            this.checkBoxUDPCS.Name = "checkBoxUDPCS";
            this.checkBoxUDPCS.Size = new System.Drawing.Size(76, 17);
            this.checkBoxUDPCS.TabIndex = 18;
            this.checkBoxUDPCS.Text = "Checksum";
            this.checkBoxUDPCS.UseVisualStyleBackColor = true;
            this.checkBoxUDPCS.CheckedChanged += new System.EventHandler(this.checkBoxUDPCS_CheckedChanged);
            // 
            // textBoxUDPLen
            // 
            this.textBoxUDPLen.Enabled = false;
            this.textBoxUDPLen.Location = new System.Drawing.Point(124, 52);
            this.textBoxUDPLen.Name = "textBoxUDPLen";
            this.textBoxUDPLen.Size = new System.Drawing.Size(62, 20);
            this.textBoxUDPLen.TabIndex = 18;
            this.textBoxUDPLen.Text = "0";
            this.textBoxUDPLen.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxUDPCS
            // 
            this.textBoxUDPCS.Enabled = false;
            this.textBoxUDPCS.Location = new System.Drawing.Point(124, 79);
            this.textBoxUDPCS.Name = "textBoxUDPCS";
            this.textBoxUDPCS.Size = new System.Drawing.Size(62, 20);
            this.textBoxUDPCS.TabIndex = 17;
            this.textBoxUDPCS.Text = "0";
            this.textBoxUDPCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxUDPLen
            // 
            this.checkBoxUDPLen.AutoSize = true;
            this.checkBoxUDPLen.Location = new System.Drawing.Point(3, 54);
            this.checkBoxUDPLen.Name = "checkBoxUDPLen";
            this.checkBoxUDPLen.Size = new System.Drawing.Size(59, 17);
            this.checkBoxUDPLen.TabIndex = 17;
            this.checkBoxUDPLen.Text = "Length";
            this.checkBoxUDPLen.UseVisualStyleBackColor = true;
            this.checkBoxUDPLen.CheckedChanged += new System.EventHandler(this.checkBoxUDPLen_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(81, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Destination port";
            // 
            // textBoxUDPDest
            // 
            this.textBoxUDPDest.Location = new System.Drawing.Point(124, 26);
            this.textBoxUDPDest.Name = "textBoxUDPDest";
            this.textBoxUDPDest.Size = new System.Drawing.Size(62, 20);
            this.textBoxUDPDest.TabIndex = 23;
            this.textBoxUDPDest.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 20;
            this.label16.Text = "Source port";
            // 
            // textBoxUDPSource
            // 
            this.textBoxUDPSource.Location = new System.Drawing.Point(124, 3);
            this.textBoxUDPSource.Name = "textBoxUDPSource";
            this.textBoxUDPSource.Size = new System.Drawing.Size(62, 20);
            this.textBoxUDPSource.TabIndex = 21;
            this.textBoxUDPSource.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabPageICMP
            // 
            this.tabPageICMP.BackColor = System.Drawing.Color.Transparent;
            this.tabPageICMP.Controls.Add(this.label20);
            this.tabPageICMP.Controls.Add(this.textBoxICMPCode);
            this.tabPageICMP.Controls.Add(this.label17);
            this.tabPageICMP.Controls.Add(this.textBoxICMPSeq);
            this.tabPageICMP.Controls.Add(this.label18);
            this.tabPageICMP.Controls.Add(this.textBoxICMPIdent);
            this.tabPageICMP.Controls.Add(this.checkBoxICMPCS);
            this.tabPageICMP.Controls.Add(this.textBoxICMPCS);
            this.tabPageICMP.Controls.Add(this.groupBox11);
            this.tabPageICMP.Location = new System.Drawing.Point(4, 22);
            this.tabPageICMP.Name = "tabPageICMP";
            this.tabPageICMP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageICMP.Size = new System.Drawing.Size(192, 371);
            this.tabPageICMP.TabIndex = 2;
            this.tabPageICMP.Text = "ICMP";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(9, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 25;
            this.label20.Text = "Code";
            // 
            // textBoxICMPCode
            // 
            this.textBoxICMPCode.Location = new System.Drawing.Point(127, 51);
            this.textBoxICMPCode.Name = "textBoxICMPCode";
            this.textBoxICMPCode.Size = new System.Drawing.Size(62, 20);
            this.textBoxICMPCode.TabIndex = 26;
            this.textBoxICMPCode.Text = "0";
            this.textBoxICMPCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 122);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 13);
            this.label17.TabIndex = 23;
            this.label17.Text = "Seq number";
            // 
            // textBoxICMPSeq
            // 
            this.textBoxICMPSeq.Location = new System.Drawing.Point(127, 119);
            this.textBoxICMPSeq.Name = "textBoxICMPSeq";
            this.textBoxICMPSeq.Size = new System.Drawing.Size(62, 20);
            this.textBoxICMPSeq.TabIndex = 24;
            this.textBoxICMPSeq.Text = "1";
            this.textBoxICMPSeq.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 100);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 21;
            this.label18.Text = "Identifier";
            // 
            // textBoxICMPIdent
            // 
            this.textBoxICMPIdent.Location = new System.Drawing.Point(127, 96);
            this.textBoxICMPIdent.Name = "textBoxICMPIdent";
            this.textBoxICMPIdent.Size = new System.Drawing.Size(62, 20);
            this.textBoxICMPIdent.TabIndex = 22;
            this.textBoxICMPIdent.Text = "1";
            this.textBoxICMPIdent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxICMPCS
            // 
            this.checkBoxICMPCS.AutoSize = true;
            this.checkBoxICMPCS.Location = new System.Drawing.Point(6, 76);
            this.checkBoxICMPCS.Name = "checkBoxICMPCS";
            this.checkBoxICMPCS.Size = new System.Drawing.Size(76, 17);
            this.checkBoxICMPCS.TabIndex = 20;
            this.checkBoxICMPCS.Text = "Checksum";
            this.checkBoxICMPCS.UseVisualStyleBackColor = true;
            this.checkBoxICMPCS.CheckedChanged += new System.EventHandler(this.checkBoxICMPCS_CheckedChanged);
            // 
            // textBoxICMPCS
            // 
            this.textBoxICMPCS.Enabled = false;
            this.textBoxICMPCS.Location = new System.Drawing.Point(127, 74);
            this.textBoxICMPCS.Name = "textBoxICMPCS";
            this.textBoxICMPCS.Size = new System.Drawing.Size(62, 20);
            this.textBoxICMPCS.TabIndex = 19;
            this.textBoxICMPCS.Text = "0";
            this.textBoxICMPCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radioButton2);
            this.groupBox11.Controls.Add(this.radioButtonRequest);
            this.groupBox11.Location = new System.Drawing.Point(6, 7);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(141, 44);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Echo";
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(77, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(52, 17);
            this.radioButton2.TabIndex = 2;
            this.radioButton2.Text = "Reply";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButtonRequest
            // 
            this.radioButtonRequest.AutoSize = true;
            this.radioButtonRequest.Checked = true;
            this.radioButtonRequest.Location = new System.Drawing.Point(6, 19);
            this.radioButtonRequest.Name = "radioButtonRequest";
            this.radioButtonRequest.Size = new System.Drawing.Size(65, 17);
            this.radioButtonRequest.TabIndex = 1;
            this.radioButtonRequest.TabStop = true;
            this.radioButtonRequest.Text = "Request";
            this.radioButtonRequest.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxData);
            this.groupBox1.Location = new System.Drawing.Point(238, 459);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(196, 129);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data";
            // 
            // textBoxData
            // 
            this.textBoxData.Location = new System.Drawing.Point(7, 19);
            this.textBoxData.Multiline = true;
            this.textBoxData.Name = "textBoxData";
            this.textBoxData.Size = new System.Drawing.Size(183, 102);
            this.textBoxData.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbAdapters);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(629, 40);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Network adaters";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.Repeat);
            this.groupBox3.Controls.Add(this.textBoxNumOfRepeat);
            this.groupBox3.Controls.Add(this.buttonDelete);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.textBoxDelay);
            this.groupBox3.Controls.Add(this.buttonSendAll);
            this.groupBox3.Controls.Add(this.buttonLoad);
            this.groupBox3.Controls.Add(this.buttonSave);
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.Controls.Add(this.buttonSendPacket);
            this.groupBox3.Controls.Add(this.listBoxPacket);
            this.groupBox3.Location = new System.Drawing.Point(441, 123);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 465);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Packet list";
            // 
            // Repeat
            // 
            this.Repeat.AutoSize = true;
            this.Repeat.Location = new System.Drawing.Point(9, 313);
            this.Repeat.Name = "Repeat";
            this.Repeat.Size = new System.Drawing.Size(74, 13);
            this.Repeat.TabIndex = 24;
            this.Repeat.Text = "Num of repeat";
            // 
            // textBoxNumOfRepeat
            // 
            this.textBoxNumOfRepeat.Location = new System.Drawing.Point(94, 313);
            this.textBoxNumOfRepeat.Name = "textBoxNumOfRepeat";
            this.textBoxNumOfRepeat.Size = new System.Drawing.Size(85, 20);
            this.textBoxNumOfRepeat.TabIndex = 23;
            this.textBoxNumOfRepeat.Text = "1";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 290);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Delay, ms";
            // 
            // textBoxDelay
            // 
            this.textBoxDelay.Location = new System.Drawing.Point(79, 287);
            this.textBoxDelay.Name = "textBoxDelay";
            this.textBoxDelay.Size = new System.Drawing.Size(101, 20);
            this.textBoxDelay.TabIndex = 21;
            this.textBoxDelay.Text = "20";
            // 
            // buttonSendAll
            // 
            this.buttonSendAll.Location = new System.Drawing.Point(113, 339);
            this.buttonSendAll.Name = "buttonSendAll";
            this.buttonSendAll.Size = new System.Drawing.Size(77, 23);
            this.buttonSendAll.TabIndex = 20;
            this.buttonSendAll.Text = "Send All";
            this.buttonSendAll.UseVisualStyleBackColor = true;
            this.buttonSendAll.Click += new System.EventHandler(this.buttonSendAll_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(113, 258);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(75, 23);
            this.buttonLoad.TabIndex = 19;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = true;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(7, 259);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 18;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxPacketName);
            this.groupBox6.Controls.Add(this.buttonBuild);
            this.groupBox6.Location = new System.Drawing.Point(7, 385);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(187, 72);
            this.groupBox6.TabIndex = 16;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "PacketName";
            // 
            // textBoxPacketName
            // 
            this.textBoxPacketName.Location = new System.Drawing.Point(6, 19);
            this.textBoxPacketName.Name = "textBoxPacketName";
            this.textBoxPacketName.Size = new System.Drawing.Size(175, 20);
            this.textBoxPacketName.TabIndex = 15;
            this.textBoxPacketName.Text = "NewPacket";
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(8, 362);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 17;
            this.buttonDelete.Text = "Remove";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonSendPacket
            // 
            this.buttonSendPacket.Location = new System.Drawing.Point(8, 339);
            this.buttonSendPacket.Name = "buttonSendPacket";
            this.buttonSendPacket.Size = new System.Drawing.Size(75, 23);
            this.buttonSendPacket.TabIndex = 14;
            this.buttonSendPacket.Text = "Send";
            this.buttonSendPacket.UseVisualStyleBackColor = true;
            this.buttonSendPacket.Click += new System.EventHandler(this.buttonSendPacket_Click);
            // 
            // listBoxPacket
            // 
            this.listBoxPacket.FormattingEnabled = true;
            this.listBoxPacket.HorizontalScrollbar = true;
            this.listBoxPacket.Location = new System.Drawing.Point(7, 16);
            this.listBoxPacket.Name = "listBoxPacket";
            this.listBoxPacket.Size = new System.Drawing.Size(187, 238);
            this.listBoxPacket.TabIndex = 9;
            // 
            // groupMac
            // 
            this.groupMac.Controls.Add(this.macAdrComboBox);
            this.groupMac.Location = new System.Drawing.Point(441, 70);
            this.groupMac.Name = "groupMac";
            this.groupMac.Size = new System.Drawing.Size(200, 47);
            this.groupMac.TabIndex = 13;
            this.groupMac.TabStop = false;
            this.groupMac.Text = "Destination MAC";
            // 
            // macAdrComboBox
            // 
            this.macAdrComboBox.Location = new System.Drawing.Point(7, 17);
            this.macAdrComboBox.Name = "macAdrComboBox";
            this.macAdrComboBox.Size = new System.Drawing.Size(187, 21);
            this.macAdrComboBox.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(113, 362);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(76, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Remove All";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(651, 598);
            this.Controls.Add(this.groupMac);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControlProtocol);
            this.Controls.Add(this.tabControl1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "Form1";
            this.Text = "GrovixPacketSender";
            this.tabControl1.ResumeLayout(false);
            this.tabPageIP.ResumeLayout(false);
            this.tabPageIP.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabControlProtocol.ResumeLayout(false);
            this.tabPageTCP.ResumeLayout(false);
            this.tabPageTCP.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.tabPageUDP.ResumeLayout(false);
            this.tabPageUDP.PerformLayout();
            this.tabPageICMP.ResumeLayout(false);
            this.tabPageICMP.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupMac.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbAdapters;
        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPageIP;
        private System.Windows.Forms.TextBox textBoxVersion;
        private System.Windows.Forms.TabControl tabControlProtocol;
        private System.Windows.Forms.TabPage tabPageTCP;
        private System.Windows.Forms.TabPage tabPageUDP;
        private System.Windows.Forms.TabPage tabPageICMP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxHeaderLength;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxData;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxReliability;
        private System.Windows.Forms.CheckBox checkBoxThroughput;
        private System.Windows.Forms.CheckBox checkBoxDelay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxIpIdent;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxIpLen;
        private System.Windows.Forms.CheckBox checkBoxIpLen;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox checkBoxIPCS;
        private System.Windows.Forms.TextBox textBoxIPCS;
        private System.Windows.Forms.TextBox textBoxTTL;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox checkBoxMF;
        private System.Windows.Forms.CheckBox checkBoxDF;
        private System.Windows.Forms.CheckBox checkBoxReserved;
        private System.Windows.Forms.GroupBox groupMac;
        private System.Windows.Forms.ComboBox macAdrComboBox;
        private System.Windows.Forms.GroupBox groupBox8;
        private IPAddressControlLib.IPAddressControl ipAddressDestination;
        private IPAddressControlLib.IPAddressControl ipAddressSource;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxTCPAck;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxTCPSeq;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxTCPDestination;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxTCPSource;
        private System.Windows.Forms.TextBox textBoxTCPOffset;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox checkBoxR0;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.CheckBox checkBoxFIN;
        private System.Windows.Forms.CheckBox checkBoxSYN;
        private System.Windows.Forms.CheckBox checkBoxRST;
        private System.Windows.Forms.CheckBox checkBoxPSH;
        private System.Windows.Forms.CheckBox checkBoxACK;
        private System.Windows.Forms.CheckBox checkBoxURG;
        private System.Windows.Forms.CheckBox checkBoxR2;
        private System.Windows.Forms.CheckBox checkBoxR1;
        private System.Windows.Forms.TextBox textBoxTCPCS;
        private System.Windows.Forms.TextBox textBoxTCPWindow;
        private System.Windows.Forms.CheckBox checkBoxTCPCS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxTCPUrgent;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox checkBoxUDPCS;
        private System.Windows.Forms.TextBox textBoxUDPLen;
        private System.Windows.Forms.TextBox textBoxUDPCS;
        private System.Windows.Forms.CheckBox checkBoxUDPLen;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxUDPDest;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxUDPSource;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButtonRequest;
        private System.Windows.Forms.CheckBox checkBoxICMPCS;
        private System.Windows.Forms.TextBox textBoxICMPCS;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxICMPSeq;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxICMPIdent;
        private System.Windows.Forms.Button buttonSendPacket;
        private System.Windows.Forms.ListBox listBoxPacket;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox textBoxPacketName;
        private System.Windows.Forms.ComboBox comboBoxECN;
        private System.Windows.Forms.ComboBox comboBoxPrecendence;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.TextBox textBoxIpProtNum;
        private System.Windows.Forms.CheckBox checkBoxIpProtNum;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxOffset;
        private System.Windows.Forms.CheckBox checkBoxNS;
        private System.Windows.Forms.CheckBox checkBoxECE;
        private System.Windows.Forms.CheckBox checkBoxCWR;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBoxICMPCode;
        private System.Windows.Forms.CheckBox checkBoxTCPOffset;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxDelay;
        private System.Windows.Forms.Button buttonSendAll;
        private System.Windows.Forms.Label Repeat;
        private System.Windows.Forms.TextBox textBoxNumOfRepeat;
        private System.Windows.Forms.Button button1;
    }
}

