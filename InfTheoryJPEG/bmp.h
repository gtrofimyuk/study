#ifndef BMP_H
#define BMP_H

#include <fstream>
#include <iostream>

typedef unsigned __int16 WORD;
typedef unsigned __int32 DWORD;
typedef unsigned char BYTE;
typedef long LONG;

#pragma pack(push, 2)
struct BMPHeader
{
	WORD    bfType;        // must be 'BM' 
	DWORD   bfSize;        // size of the whole .bmp file
	WORD    bfReserved1;   // must be 0
	WORD    bfReserved2;   // must be 0
	DWORD   bfOffBits;	   // position where actual data starts
};

struct BMPInfoHeader
{
	DWORD  biSize;            // size of the structure
	LONG   biWidth;           // image width
	LONG   biHeight;          // image height
	WORD   biPlanes;          // bitplanes
	WORD   biBitCount;        // resolution 
	DWORD  biCompression;     // compression
	DWORD  biSizeImage;       // size of the image
	LONG   biXPelsPerMeter;   // pixels per meter X
	LONG   biYPelsPerMeter;   // pixels per meter Y
	DWORD  biClrUsed;         // colors used
	DWORD  biClrImportant;    // important colors
};
#pragma pack(pop)
#pragma pack(push, 1)
struct RGB
{
	BYTE red;
	BYTE green;
	BYTE blue;
};
#pragma pack(pop)

RGB* openConvert(const char *fileName, unsigned& height, unsigned& width, unsigned long long& size);
#endif